# gui imports
from Qt.QtGui import QIcon
from Qt.QtWidgets import QDialog

# nebula imports
from nebula.auth import user
from nebula.common import gui

# python imports
import os


# import the compiled ui form
from ..ui.dialog_ui import Ui_Dialog

# construct commonly used paths
rootPath = os.path.dirname(os.path.dirname(__file__))
iconPath = os.path.join(rootPath, 'icons')


class Dialog(Ui_Dialog, QDialog):
    def __init__(self, parent=gui.getMayaWindow()):
        # get the ui form
        super(Dialog, self).__init__(parent)
        self.setupUi(self)
        self.username = os.environ['USERNAME']
        self.success = True
        self.setWindowIcon(QIcon(os.path.join(iconPath, 'login.png')))
        self.setUsername()
        self.passwordBox.setFocus()

        # connections
        self.loginButton.clicked.connect(self.login)
        self.cancelButton.clicked.connect(self.closeWindow)

    def setUsername(self):
        self.usernameBox.setText(self.username)

    def login(self):
        username = str(self.usernameBox.text())
        password = str(self.passwordBox.text())
        #user.login(username, password)
        try:
            user.login(username, password)
            self.accept()
        except Exception, e:
            gui.showMessage(self, title='Login', msg=str(e))

    def closeWindow(self):
        self.close()
        self.reject()
        self.deleteLater()

    def closeEvent(self, event):
        self.deleteLater()
